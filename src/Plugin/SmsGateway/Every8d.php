<?php

namespace Drupal\sms_every8d\Plugin\SmsGateway;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Every8d\Client;
use Every8d\Message\SMS;

/**
 * Implements every8d gateway.
 *
 * @SmsGateway(
 *   id = "every8d",
 *   label = @Translation("Every8d"),
 * )
 */
class Every8d extends SmsGatewayPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'username' => '',
      'password' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['every8d'] = [
      '#type' => 'details',
      '#title' => $this->t('Every8d'),
      '#open' => TRUE,
    ];

    $form['every8d']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config['username'],
      '#required' => TRUE,
    ];

    $form['every8d']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config['password'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['username'] = trim($form_state->getValue('username'));
    $this->configuration['password'] = trim($form_state->getValue('password'));
  }

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message) {

    $result = new SmsMessageResult();

    $username = $this->configuration['username'];
    $password = $this->configuration['password'];

    $client = new Client(['username' => $username, 'password' => $password]);

    foreach ($sms_message->getRecipients() as $recipient) {
      $report = new SmsDeliveryReport();
      try {
        $message = new SMS($recipient, $sms_message->getMessage());
        $sms_result = $client->sendSMS($message);
        $report->setRecipient($recipient);
        $report->setStatus(SmsMessageReportStatus::QUEUED);
        $report->setMessageId($sms_result['BatchID']);
      }
      catch (\Exception $e) {
        $code = $e->getCode();
        $message = $e->getMessage();

        if ($code == -306) {
          $report->setStatus(SmsMessageReportStatus::INVALID_RECIPIENT);
          $report->setStatusMessage($message);
        }
        else {
          $report->setStatus(SmsMessageReportStatus::ERROR);
          $report->setStatusMessage($message);
        }
      }

      if ($report->getStatus()) {
        $result->addReport($report);
      }
    }

    return $result;
  }

}
